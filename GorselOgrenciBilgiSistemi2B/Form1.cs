﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselOgrenciBilgiSistemi2B
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btngiris_Click(object sender, EventArgs e)
        {
            OgrenciAnasayfa ogrmain = new OgrenciAnasayfa();
            OBSEntities2 database = new OBSEntities2();
            List<OgrenciGiris> c = database.OgrenciGiris.Where(OgrenciGiris => OgrenciGiris.OgrenciNo == txtuser.Text && OgrenciGiris.Sifre == txtpass.Text).ToList();
            if (c.Count > 0)
            {
                this.Hide();

                ogrmain.ShowDialog();

            }
            else {
                label1.Text = "Kullanıcı Adı veya Şifre Yanlış";
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            YonetimPanel yon = new YonetimPanel();
            yon.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SifreHatirlatma sifre = new SifreHatirlatma();
            sifre.Show();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OgrnoOgren OgrNo = new OgrnoOgren();
            OgrNo.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
