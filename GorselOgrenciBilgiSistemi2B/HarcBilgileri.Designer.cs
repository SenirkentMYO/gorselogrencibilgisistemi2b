﻿namespace GorselOgrenciBilgiSistemi2B
{
    partial class HarcBilgileri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel23 = new System.Windows.Forms.LinkLabel();
            this.linkLabel22 = new System.Windows.Forms.LinkLabel();
            this.linkLabel21 = new System.Windows.Forms.LinkLabel();
            this.linkLabel20 = new System.Windows.Forms.LinkLabel();
            this.linkLabel19 = new System.Windows.Forms.LinkLabel();
            this.linkLabel18 = new System.Windows.Forms.LinkLabel();
            this.linkLabel17 = new System.Windows.Forms.LinkLabel();
            this.linkLabel16 = new System.Windows.Forms.LinkLabel();
            this.linkLabel15 = new System.Windows.Forms.LinkLabel();
            this.linkLabel14 = new System.Windows.Forms.LinkLabel();
            this.linkLabel13 = new System.Windows.Forms.LinkLabel();
            this.linkLabel12 = new System.Windows.Forms.LinkLabel();
            this.linkLabel11 = new System.Windows.Forms.LinkLabel();
            this.linkLabel10 = new System.Windows.Forms.LinkLabel();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel8 = new System.Windows.Forms.LinkLabel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.oBSDataSet = new GorselOgrenciBilgiSistemi2B.OBSDataSet();
            this.harcBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.harcTableAdapter = new GorselOgrenciBilgiSistemi2B.OBSDataSetTableAdapters.HarcTableAdapter();
            this.ıDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ogrNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yılıDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dönemiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.harçTipiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.borçDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tahsilatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kalanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.açıklamaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oBSDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.harcBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel23);
            this.groupBox1.Controls.Add(this.linkLabel22);
            this.groupBox1.Controls.Add(this.linkLabel21);
            this.groupBox1.Controls.Add(this.linkLabel20);
            this.groupBox1.Controls.Add(this.linkLabel19);
            this.groupBox1.Controls.Add(this.linkLabel18);
            this.groupBox1.Controls.Add(this.linkLabel17);
            this.groupBox1.Controls.Add(this.linkLabel16);
            this.groupBox1.Controls.Add(this.linkLabel15);
            this.groupBox1.Controls.Add(this.linkLabel14);
            this.groupBox1.Controls.Add(this.linkLabel13);
            this.groupBox1.Controls.Add(this.linkLabel12);
            this.groupBox1.Controls.Add(this.linkLabel11);
            this.groupBox1.Controls.Add(this.linkLabel10);
            this.groupBox1.Controls.Add(this.linkLabel9);
            this.groupBox1.Controls.Add(this.linkLabel8);
            this.groupBox1.Controls.Add(this.linkLabel7);
            this.groupBox1.Controls.Add(this.linkLabel6);
            this.groupBox1.Controls.Add(this.linkLabel5);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 463);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Menü";
            // 
            // linkLabel23
            // 
            this.linkLabel23.AutoSize = true;
            this.linkLabel23.Location = new System.Drawing.Point(6, 437);
            this.linkLabel23.Name = "linkLabel23";
            this.linkLabel23.Size = new System.Drawing.Size(51, 13);
            this.linkLabel23.TabIndex = 18;
            this.linkLabel23.TabStop = true;
            this.linkLabel23.Text = "Anasayfa";
            // 
            // linkLabel22
            // 
            this.linkLabel22.AutoSize = true;
            this.linkLabel22.Location = new System.Drawing.Point(6, 414);
            this.linkLabel22.Name = "linkLabel22";
            this.linkLabel22.Size = new System.Drawing.Size(136, 13);
            this.linkLabel22.TabIndex = 17;
            this.linkLabel22.TabStop = true;
            this.linkLabel22.Text = "Mezun/İlişik Kesme Belgesi";
            // 
            // linkLabel21
            // 
            this.linkLabel21.AutoSize = true;
            this.linkLabel21.Location = new System.Drawing.Point(6, 391);
            this.linkLabel21.Name = "linkLabel21";
            this.linkLabel21.Size = new System.Drawing.Size(82, 13);
            this.linkLabel21.TabIndex = 16;
            this.linkLabel21.TabStop = true;
            this.linkLabel21.Text = "Yaz Okulu Kayıt";
            // 
            // linkLabel20
            // 
            this.linkLabel20.AutoSize = true;
            this.linkLabel20.Location = new System.Drawing.Point(6, 363);
            this.linkLabel20.Name = "linkLabel20";
            this.linkLabel20.Size = new System.Drawing.Size(78, 13);
            this.linkLabel20.TabIndex = 15;
            this.linkLabel20.TabStop = true;
            this.linkLabel20.Text = "Sınav Programı";
            // 
            // linkLabel19
            // 
            this.linkLabel19.AutoSize = true;
            this.linkLabel19.Location = new System.Drawing.Point(6, 344);
            this.linkLabel19.Name = "linkLabel19";
            this.linkLabel19.Size = new System.Drawing.Size(79, 13);
            this.linkLabel19.TabIndex = 14;
            this.linkLabel19.TabStop = true;
            this.linkLabel19.Text = "Ön Kayıt Formu";
            // 
            // linkLabel18
            // 
            this.linkLabel18.AutoSize = true;
            this.linkLabel18.Location = new System.Drawing.Point(6, 317);
            this.linkLabel18.Name = "linkLabel18";
            this.linkLabel18.Size = new System.Drawing.Size(79, 13);
            this.linkLabel18.TabIndex = 13;
            this.linkLabel18.TabStop = true;
            this.linkLabel18.Text = "Öğrenci Bilgileri";
            // 
            // linkLabel17
            // 
            this.linkLabel17.AutoSize = true;
            this.linkLabel17.Location = new System.Drawing.Point(6, 293);
            this.linkLabel17.Name = "linkLabel17";
            this.linkLabel17.Size = new System.Drawing.Size(71, 13);
            this.linkLabel17.TabIndex = 12;
            this.linkLabel17.TabStop = true;
            this.linkLabel17.Text = "Mesaj Kutusu";
            // 
            // linkLabel16
            // 
            this.linkLabel16.AutoSize = true;
            this.linkLabel16.Location = new System.Drawing.Point(6, 270);
            this.linkLabel16.Name = "linkLabel16";
            this.linkLabel16.Size = new System.Drawing.Size(133, 13);
            this.linkLabel16.TabIndex = 11;
            this.linkLabel16.TabStop = true;
            this.linkLabel16.Text = "Kayıt Yenileme -Ders Kaydı";
            // 
            // linkLabel15
            // 
            this.linkLabel15.AutoSize = true;
            this.linkLabel15.Location = new System.Drawing.Point(6, 245);
            this.linkLabel15.Name = "linkLabel15";
            this.linkLabel15.Size = new System.Drawing.Size(65, 13);
            this.linkLabel15.TabIndex = 10;
            this.linkLabel15.TabStop = true;
            this.linkLabel15.Text = "Harç Bilgileri";
            // 
            // linkLabel14
            // 
            this.linkLabel14.AutoSize = true;
            this.linkLabel14.Location = new System.Drawing.Point(6, 222);
            this.linkLabel14.Name = "linkLabel14";
            this.linkLabel14.Size = new System.Drawing.Size(67, 13);
            this.linkLabel14.TabIndex = 9;
            this.linkLabel14.TabStop = true;
            this.linkLabel14.Text = "Hata Bildirimi";
            // 
            // linkLabel13
            // 
            this.linkLabel13.AutoSize = true;
            this.linkLabel13.Location = new System.Drawing.Point(6, 200);
            this.linkLabel13.Name = "linkLabel13";
            this.linkLabel13.Size = new System.Drawing.Size(41, 13);
            this.linkLabel13.TabIndex = 8;
            this.linkLabel13.TabStop = true;
            this.linkLabel13.Text = "Etkinlik";
            // 
            // linkLabel12
            // 
            this.linkLabel12.AutoSize = true;
            this.linkLabel12.Location = new System.Drawing.Point(6, 177);
            this.linkLabel12.Name = "linkLabel12";
            this.linkLabel12.Size = new System.Drawing.Size(79, 13);
            this.linkLabel12.TabIndex = 7;
            this.linkLabel12.TabStop = true;
            this.linkLabel12.Text = "Dönem Dersleri";
            // 
            // linkLabel11
            // 
            this.linkLabel11.AutoSize = true;
            this.linkLabel11.Location = new System.Drawing.Point(6, 155);
            this.linkLabel11.Name = "linkLabel11";
            this.linkLabel11.Size = new System.Drawing.Size(73, 13);
            this.linkLabel11.TabIndex = 6;
            this.linkLabel11.TabStop = true;
            this.linkLabel11.Text = "Ders Programı";
            // 
            // linkLabel10
            // 
            this.linkLabel10.AutoSize = true;
            this.linkLabel10.Location = new System.Drawing.Point(6, 129);
            this.linkLabel10.Name = "linkLabel10";
            this.linkLabel10.Size = new System.Drawing.Size(105, 13);
            this.linkLabel10.TabIndex = 5;
            this.linkLabel10.TabStop = true;
            this.linkLabel10.Text = "Ders İntibak Kararları";
            // 
            // linkLabel9
            // 
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.Location = new System.Drawing.Point(6, 103);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(91, 13);
            this.linkLabel9.TabIndex = 4;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "Ders Dökümanları";
            // 
            // linkLabel8
            // 
            this.linkLabel8.AutoSize = true;
            this.linkLabel8.Location = new System.Drawing.Point(6, 83);
            this.linkLabel8.Name = "linkLabel8";
            this.linkLabel8.Size = new System.Drawing.Size(64, 13);
            this.linkLabel8.TabIndex = 3;
            this.linkLabel8.TabStop = true;
            this.linkLabel8.Text = "Ders Bilgileri";
            // 
            // linkLabel7
            // 
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.Location = new System.Drawing.Point(6, 61);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(96, 13);
            this.linkLabel7.TabIndex = 2;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "Çiftanadal Başvuru";
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.Location = new System.Drawing.Point(6, 38);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(99, 13);
            this.linkLabel6.TabIndex = 1;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "Bütünleme Başvuru";
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(6, 16);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(124, 13);
            this.linkLabel5.TabIndex = 0;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "ENF-150 Muafiyet Sınavı";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(160, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(562, 463);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Harç Bilgileri";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(6, 236);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(550, 54);
            this.dataGridView2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ıDDataGridViewTextBoxColumn,
            this.ogrNoDataGridViewTextBoxColumn,
            this.yılıDataGridViewTextBoxColumn,
            this.dönemiDataGridViewTextBoxColumn,
            this.harçTipiDataGridViewTextBoxColumn,
            this.borçDataGridViewTextBoxColumn,
            this.tahsilatDataGridViewTextBoxColumn,
            this.kalanDataGridViewTextBoxColumn,
            this.açıklamaDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.harcBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 61);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(550, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "KREDİ BİLGİSİ İLE İLGİLİ KAYIT BULUNAMADI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(64, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(437, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "                                 BURS BİLGİSİ İLE İLGİLİ KAYIT BULUNAMADI        " +
    "                          \r\n";
            // 
            // oBSDataSet
            // 
            this.oBSDataSet.DataSetName = "OBSDataSet";
            this.oBSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // harcBindingSource
            // 
            this.harcBindingSource.DataMember = "Harc";
            this.harcBindingSource.DataSource = this.oBSDataSet;
            // 
            // harcTableAdapter
            // 
            this.harcTableAdapter.ClearBeforeFill = true;
            // 
            // ıDDataGridViewTextBoxColumn
            // 
            this.ıDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.ıDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.ıDDataGridViewTextBoxColumn.Name = "ıDDataGridViewTextBoxColumn";
            this.ıDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ogrNoDataGridViewTextBoxColumn
            // 
            this.ogrNoDataGridViewTextBoxColumn.DataPropertyName = "Ogr No";
            this.ogrNoDataGridViewTextBoxColumn.HeaderText = "Ogr No";
            this.ogrNoDataGridViewTextBoxColumn.Name = "ogrNoDataGridViewTextBoxColumn";
            // 
            // yılıDataGridViewTextBoxColumn
            // 
            this.yılıDataGridViewTextBoxColumn.DataPropertyName = "Yılı";
            this.yılıDataGridViewTextBoxColumn.HeaderText = "Yılı";
            this.yılıDataGridViewTextBoxColumn.Name = "yılıDataGridViewTextBoxColumn";
            // 
            // dönemiDataGridViewTextBoxColumn
            // 
            this.dönemiDataGridViewTextBoxColumn.DataPropertyName = "Dönemi";
            this.dönemiDataGridViewTextBoxColumn.HeaderText = "Dönemi";
            this.dönemiDataGridViewTextBoxColumn.Name = "dönemiDataGridViewTextBoxColumn";
            // 
            // harçTipiDataGridViewTextBoxColumn
            // 
            this.harçTipiDataGridViewTextBoxColumn.DataPropertyName = "Harç Tipi";
            this.harçTipiDataGridViewTextBoxColumn.HeaderText = "Harç Tipi";
            this.harçTipiDataGridViewTextBoxColumn.Name = "harçTipiDataGridViewTextBoxColumn";
            // 
            // borçDataGridViewTextBoxColumn
            // 
            this.borçDataGridViewTextBoxColumn.DataPropertyName = "Borç";
            this.borçDataGridViewTextBoxColumn.HeaderText = "Borç";
            this.borçDataGridViewTextBoxColumn.Name = "borçDataGridViewTextBoxColumn";
            // 
            // tahsilatDataGridViewTextBoxColumn
            // 
            this.tahsilatDataGridViewTextBoxColumn.DataPropertyName = "Tahsilat";
            this.tahsilatDataGridViewTextBoxColumn.HeaderText = "Tahsilat";
            this.tahsilatDataGridViewTextBoxColumn.Name = "tahsilatDataGridViewTextBoxColumn";
            // 
            // kalanDataGridViewTextBoxColumn
            // 
            this.kalanDataGridViewTextBoxColumn.DataPropertyName = "Kalan";
            this.kalanDataGridViewTextBoxColumn.HeaderText = "Kalan";
            this.kalanDataGridViewTextBoxColumn.Name = "kalanDataGridViewTextBoxColumn";
            // 
            // açıklamaDataGridViewTextBoxColumn
            // 
            this.açıklamaDataGridViewTextBoxColumn.DataPropertyName = "Açıklama";
            this.açıklamaDataGridViewTextBoxColumn.HeaderText = "Açıklama";
            this.açıklamaDataGridViewTextBoxColumn.Name = "açıklamaDataGridViewTextBoxColumn";
            // 
            // HarcBilgileri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 484);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "HarcBilgileri";
            this.Text = "HarcBilgileri";
            this.Load += new System.EventHandler(this.HarcBilgileri_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oBSDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.harcBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.LinkLabel linkLabel23;
        private System.Windows.Forms.LinkLabel linkLabel22;
        private System.Windows.Forms.LinkLabel linkLabel21;
        private System.Windows.Forms.LinkLabel linkLabel20;
        private System.Windows.Forms.LinkLabel linkLabel19;
        private System.Windows.Forms.LinkLabel linkLabel18;
        private System.Windows.Forms.LinkLabel linkLabel17;
        private System.Windows.Forms.LinkLabel linkLabel16;
        private System.Windows.Forms.LinkLabel linkLabel15;
        private System.Windows.Forms.LinkLabel linkLabel14;
        private System.Windows.Forms.LinkLabel linkLabel13;
        private System.Windows.Forms.LinkLabel linkLabel12;
        private System.Windows.Forms.LinkLabel linkLabel11;
        private System.Windows.Forms.LinkLabel linkLabel10;
        private System.Windows.Forms.LinkLabel linkLabel9;
        private System.Windows.Forms.LinkLabel linkLabel8;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private OBSDataSet oBSDataSet;
        private System.Windows.Forms.BindingSource harcBindingSource;
        private OBSDataSetTableAdapters.HarcTableAdapter harcTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ıDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ogrNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yılıDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dönemiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn harçTipiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn borçDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tahsilatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kalanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn açıklamaDataGridViewTextBoxColumn;
    }
}