﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselOgrenciBilgiSistemi2B
{
    public partial class HarcBilgileri : Form
    {
        public HarcBilgileri()
        {
            InitializeComponent();
        }

        private void HarcBilgileri_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oBSDataSet.Harc' table. You can move, or remove it, as needed.
            this.harcTableAdapter.Fill(this.oBSDataSet.Harc);

        }
    }
}
