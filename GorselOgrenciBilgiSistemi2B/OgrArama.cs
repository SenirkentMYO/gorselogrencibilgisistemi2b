﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselOgrenciBilgiSistemi2B
{
    public partial class OgrArama : Form
    {
        public OgrArama()
        {
            InitializeComponent();
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            OBSEntities2 db = new OBSEntities2();
            if (textBox4.Text.Length > 0)
            {
                List<OgrenciGiris> liste = db.OgrenciGiris.Where(a => a.Ad.Contains(textBox4.Text)).ToList();
                dataGridView2.DataSource = liste;

            }
            else if (textBox5.Text.Length > 0)
            {
                List<OgrenciGiris> liste = db.OgrenciGiris.Where(a => a.Soyad.Contains(textBox5.Text)).ToList();
                dataGridView2.DataSource = liste;
            }
            else if (textBox6.Text.Length > 0)
            {
                List<OgrenciGiris> liste = db.OgrenciGiris.Where(a => a.OgrenciNo.Contains(textBox6.Text)).ToList();
                dataGridView2.DataSource = liste;
            }
            else
            {
                MessageBox.Show("Aramak için bir değer girin");
            }
          //  List<OgrenciGiris> liste = db.OgrenciGiris.Where(a => a.Ad.Contains(textBox4.Text) || a.Soyad.Contains(textBox5.text) || a.OgrenciNo.Contains(textBox6.Text)).ToList();

           // dataGridView1.DataSource = liste;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            YonetimGirisi y = new YonetimGirisi();
            y.Show();
        }
    }
}
