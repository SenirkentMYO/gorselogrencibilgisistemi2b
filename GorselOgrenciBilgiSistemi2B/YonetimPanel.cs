﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GorselOgrenciBilgiSistemi2B
{
    public partial class YonetimPanel : Form
    {
        public YonetimPanel()
        {
            InitializeComponent();
        }

        private void btnGiris_Click(object sender, EventArgs e)
        {
            YonetimGirisi ss = new YonetimGirisi();
            OBSEntities2 database = new OBSEntities2();
            List<KullaniciGiris> c = database.KullaniciGiris.Where(KullaniciGiris => KullaniciGiris.Yonetici == textBox1.Text && KullaniciGiris.Sifre == textBox2.Text).ToList();


            if (c.Count > 0)
            {
                this.Hide();

                ss.ShowDialog();

            }
            else
            {
                MessageBox.Show("giriş hatalı");
            }
        }
    }
}
